
// computing quartiles
#include <iostream>
#include <string>
#include <iomanip>
#include <ios>
#include <algorithm>
#include <vector>

// say what standard-library names we use
using std::cin;
using std::cout;
using std::endl;
using std::string;
using std::setprecision;
using std::streamsize;
using std::sort;
using std::vector;

int main()
{
	
	// ask for some numbers on which to compute quartiles
	cout << "Enter some numbers: ";

	// a variable int which to read
	vector<double> numbers;
	double x;
	

	// invariant:
	// numbers contains all the numbers read so far
	while (cin >> x)
	{
		numbers.push_back(x);
	}

	//check that the numbers have been provided
	typedef vector<double>::size_type vec_sz;
	vec_sz size = numbers.size();

	if (size == 0)
	{
		cout << endl << "You must enter at least 4 numbers. Please try again." << endl;
		return 1;
	}

	// sort the numbers
	sort(numbers.begin(), numbers.end());

	// compute the indices to pick the values from
	const int q1 = size / 4;
	const int q2 = size / 2;
	const int q3 = q1 + q2;
	//double q1val, q2val q3val;
	
	nth_element(numbers.begin(), numbers.begin() + q1, numbers.end());
	nth_element(numbers.begin() + q1 + 1, numbers.begin() + q2, numbers.end());
	nth_element(numbers.begin() + q2 + 1, numbers.begin() + q3, numbers.end());



	// write the result
	streamsize prec = cout.precision();
	cout << "The quartiles are: " << setprecision(3)
		<< numbers[q1] << ", " << numbers[q2] << ", " << numbers[q3] << ", " << numbers[size - 1]
		<< setprecision(prec) << endl;

	return 0;
}