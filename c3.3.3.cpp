
// counting how many times each distinct word appears
#include <iostream>
#include <string>
#include <iomanip>
#include <ios>
#include <algorithm>
#include <vector>

// say what standard-library names we use
using std::cin;
using std::cout;
using std::endl;
using std::string;
using std::setprecision;
using std::streamsize;
using std::sort;
using std::vector;
using std::to_string;

int main()
{
	
	// ask for some words
	cout << "Enter some words: ";

	// a variable int which to read
	vector<string> words;
	string x;
	//string sentence = "";

	// a vector that stores the counts of the words in respective position.
	vector<int> counts;
	
	//sentence = getline(cin, sentence);

	// invariant:
	// words contains all the words read so far
	while (cin >> x)
	{
		// if this is the first word then simply add to words vector
		if (words.size() == 0)
		{
			words.push_back(x);	
			counts.push_back(1);
		} else
		{
			// check if this word is already in our list. If it is then increment the 
			// respective count of this word in the counts vector.
			// If word does not exist then add this to the vector and make corresponding
			// count entry in the counts vector.
			vector<string>::iterator itw;
			itw = find(words.begin(), words.end(), x);
			// if iterator is not pointing to last then we have encountered this word
			// already and we have to simply increment the respective count.
			if (itw != words.end())
			{
				// get the position using the iterator
				int pos = itw - words.begin();

				counts[pos] = counts[pos] + 1;

			} else // this is a new word. Add it to words vector and the counts vector.
			{
				words.push_back(x);	
				counts.push_back(1);
			}
		}
		
		
	}

	//check that the numbers have been provided
	typedef vector<string>::size_type vec_sz;
	vec_sz size = words.size();

	if (size == 0)
	{
		cout << endl << "You must enter at least some words. Please try again." << endl;
		return 1;
	}

	string resultstring = "";

	// Prepare the output result string
	
	for (int i=0; i != size; ++i)
	{
		resultstring += words[i] + ": " + to_string(counts[i]) + ", ";
	}
	
	

	// write the result
	streamsize prec = cout.precision();
	cout << "You typed following distinct words > " << setprecision(3)
		<< resultstring
		<< setprecision(prec) << endl;

	return 0;
}