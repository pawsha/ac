// organizing data
#include <iostream>
#include <string>
#include <iomanip>
#include <ios>
#include <algorithm>
#include <vector>
#include <stdexcept>

// say what standard-library names we use
using std::cin;
using std::cout;
using std::endl;
using std::string;
using std::setprecision;
using std::streamsize;
using std::sort;
using std::vector;
using std::to_string;
using std::domain_error;
using std::istream;
using std::max;

//structure to hold the student info
struct Student_Info
{
	string name;
	double midterm;
	double final;
	vector<double> homework;
};

// compute the median of a vector<double>
// note that calling this function copies the entire argument vector
double median(vector<double> vec)
{
	typedef vector<double>::size_type vec_sz;
	vec_sz size = vec.size();
	
	if (size ==0)
		throw domain_error("median of an empty vector");

	sort(vec.begin(), vec.end());
	
	vec_sz mid = size/2;
	
	return size % 2 == 0 ? (vec[mid] + vec[mid - 1]) / 2 : vec[mid];

}

// compute a student's overall grade from midterm, final and homework grades
double grade(double midterm, double final, double homework)
{
	return 0.2 * midterm + 0.4 * final + 0.4 * homework;
}

// compute the overall grade from midterm and final grades
// and a vector of homework grades
// this function does not copy its ardgument of vector, because median does so for us
double grade(double midterm, double final, const vector<double>& hw)
{
	if (hw.size() == 0)
		throw domain_error("Student has done no homework");

	return grade(midterm, final, median(hw));
}

// compute grade, accept struct Student_Info
double grade (const Student_Info& s)
{
	return grade(s.midterm, s.final, s.homework);
}

//read homework grades from an input stream into a vector<double>
istream& read_hw(istream& in, vector<double>& hw)
{
	if (in)
	{
		//get rid of previous contents
		hw.clear();

		// read homework grades
		double x;
		while (cin >> x)
		{
			hw.push_back(x);
		}

		//clear the stream so that the input will work for the next student
		in.clear();
	}

	return in;
}

//read student info
istream& read(istream& is, Student_Info& s)
{
	//read and store the student's name, midterm, final and homework grades
	is >> s.name >> s.midterm >> s.final;
	read_hw(is, s.homework);
	return is;
	
}

//compare function to compare Students on 
bool compare(const Student_Info& x, const Student_Info& y)
{
	return x.name < y.name;
}

int main()
{

	
	//ask for students' grades
	cout << "Please enter all students' name and grades followed by end of file:  \n";

	vector<Student_Info> students;
	Student_Info record;
	string::size_type maxlen = 0;

	//read and store all the records and find the length of the longest name
	while (read(cin, record))
	{
		maxlen = max(maxlen, record.name.size());
		students.push_back(record);
	}

	//alphabetize the students
	sort(students.begin(), students.end(), compare);

	//compute and generate the overall grade, if possible, for all students
	for (vector<Student_Info>::size_type i = 0; i != students.size(); ++i)
	{
		//write the name, padded to the right with spaces maxlen + 1
		cout << students[i].name
				<< string(maxlen + 1 - students[i].name.size(), ' ');

		try
		{
			double final_grade = grade(students[i]);
			streamsize prec = cout.precision();
			cout << setprecision(3) << final_grade << setprecision(prec);
		} catch (domain_error e)
		{
			cout << e.what();
			return 1;
		}

		cout << endl;
	}
	
	return 0;
}

