
#include <iostream>
#include <iomanip>
#include <ios>
#include "Student_info.h"

using std::string;
using std::vector;
using std::istream;
using std::cin;

//compare function to compare Students on 
bool compare(const Student_Info& x, const Student_Info& y)
{
	return x.name < y.name;
}

//read homework grades from an input stream into a vector<double>
istream& read_hw(istream& in, vector<double>& hw)
{
	if (in)
	{
		//get rid of previous contents
		hw.clear();

		// read homework grades
		double x;
		while (cin >> x)
		{
			hw.push_back(x);
		}

		//clear the stream so that the input will work for the next student
		in.clear();
	}

	return in;
}

//read student info
istream& read(istream& is, Student_Info& s)
{
	//read and store the student's name, midterm, final and homework grades
	is >> s.name >> s.midterm >> s.final;
	read_hw(is, s.homework);
	return is;
	
}

