#ifndef GUARD_Student_info_h
#define GUARD_Student_info_h

//Student_info.h
#include <iostream>
#include <iomanip>
#include <ios>
#include <string>
#include <vector>

//structure to hold the student info
struct Student_Info
{
	std::string name;
	double midterm;
	double final;
	std::vector<double> homework;
};

bool compare(const Student_Info&, const Student_Info&);
std::istream& read_hw(std::istream&, std::vector<double>&);
std::istream& read(std::istream&, Student_Info&);


#endif