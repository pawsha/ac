// organizing data
#include <iostream>
#include <string>
#include <iomanip>
#include <ios>
#include <algorithm>
#include <vector>
#include <stdexcept>
#include "Student_info.h"
#include "grade.h"

// say what standard-library names we use
using std::cin;
using std::cout;
using std::endl;
using std::string;
using std::setprecision;
using std::streamsize;
using std::sort;
using std::vector;
using std::to_string;
using std::domain_error;
using std::istream;
using std::max;

int main()
{

	
	//ask for students' grades
	cout << "Please enter all students' name and grades followed by end of file:  \n";

	vector<Student_Info> students;
	Student_Info record;
	string::size_type maxlen = 0;

	//read and store all the records and find the length of the longest name
	while (read(cin, record))
	{
		maxlen = max(maxlen, record.name.size());
		students.push_back(record);
	}

	//alphabetize the students
	sort(students.begin(), students.end(), compare);

	//compute and generate the overall grade, if possible, for all students
	for (vector<Student_Info>::size_type i = 0; i != students.size(); ++i)
	{
		//write the name, padded to the right with spaces maxlen + 1
		cout << students[i].name
				<< string(maxlen + 1 - students[i].name.size(), ' ');

		try
		{
			double final_grade = grade(students[i]);
			streamsize prec = cout.precision();
			cout << setprecision(3) << final_grade << setprecision(prec);
		} catch (domain_error e)
		{
			cout << e.what();
			return 1;
		}

		cout << endl;
	}
	
	return 0;
}

