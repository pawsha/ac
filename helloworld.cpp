#include <iostream>

/*
int main(){

	std::cout << "This is a (\") quote and this is a (\\) backslash \t." << std::endl;

	return 0;

}
*/

//int main(){{{{{{std::cout << "This is a (\") quote and this is a (\\) backslash \t." << std::endl;}}}}}}


int main(){

	// This is a comment that extends over several lines
	// because it uses /* adnd */ as its starting and /*
	// or */ to delimit the comments.

	std::cout << "Hello,\nWorld!" << std::endl;
	return 0;

 }